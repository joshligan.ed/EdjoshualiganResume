* {
  max-width: 1000px;
  margin: auto;
}
.body{
  margin: auto;
  padding: auto;
  font-family: "Montserrat",sans-serif;
}
.resume{
  background: #0a1116;
  padding: 100px;
  text-align: center;
}
.resume h1{
  text-align: justify;
  color: white;
  font-size: 12px;
}
.pic{
  width: 160px;
  height: 160px;
  position: relative;
  margin: auto;
}
.pic img{
  width: 100%;
  height: 100%;
  border-radius: 50%;
}
.resume h2{
  text-transform: uppercase;
  color: gray;
}
.resume p{
  width: 800px;
  margin: auto;
  color: #f1f1f1;
  text-align: justify;
  font: 24px;  
}
.resume h3{
 text-transform: uppercase;
 color: bisque;
 text-align: initial;
}

